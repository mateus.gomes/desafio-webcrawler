#!/usr/bin/env python3

from lib.request import Request
from lib.parsers.baseParser import BaseParser

class JediCrawler:
    def __init__(self, temp, parser=None):
        self.__temp = temp
        self.__parser = parser

    def action(self):
        response = Request().request(self.__temp.url, self.__temp.httpMethod, self.__temp.fields)

        if response.status != 200:
            raise Exception('response.status != 200')
        
        if self.__parser != None:
            return self.__parser.parse(response)


class Temp:
    def __init__(self, url, httpMethod, fields=None):
        self.url = url
        self.httpMethod = httpMethod
        self.fields = fields