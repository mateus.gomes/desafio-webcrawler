#!/usr/bin/env python3

import urllib3

class Request:
    def __init__(self):
        self.__httpPool = urllib3.PoolManager()

    def request(self, url, httpMethod, fields=None):
        response = self.__httpPool.request(httpMethod, url, fields=fields, retries=3)
        return response
