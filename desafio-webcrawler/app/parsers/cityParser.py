#!/usr/bin/env python3

from bs4 import BeautifulSoup
from lib.parsers.baseParser import BaseParser
from app.models.cityModel import CityModel

class CityParser(BaseParser):
    def __init__(self):
        BaseParser.__init__(self)

    def parse(self, response):
        soup = BeautifulSoup(response.data, 'html.parser')
        cities = soup.select('select[id=location__select] > option[data-utm]')
        
        citiesModels = []

        for city in cities:
            citiesModels.append(CityModel(city.get("data-utm"), city.get("value")))

        return citiesModels