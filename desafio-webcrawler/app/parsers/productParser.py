#!/usr/bin/env python3

from bs4 import BeautifulSoup
from lib.parsers.baseParser import BaseParser
from app.models.productModel import ProductModel

class ProductParser(BaseParser):
    def __init__(self):
        BaseParser.__init__(self)

    def parse(self, response):
        soup = BeautifulSoup(response.data, 'html.parser')

        url = response._request_url
        img_url = soup.find('a', {'id': 'botaoZoom'}).get('rel')[0]
        name = soup.find('div', {'class': 'productName'}).contents[0]
        price = soup.find('input', {'id': '___rc-p-dv-id'}).get('value')
        availability = True
        
        return ProductModel(name, url, img_url, price, availability)