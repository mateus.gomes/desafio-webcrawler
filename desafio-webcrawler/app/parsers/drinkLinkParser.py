#!/usr/bin/env python3

from bs4 import BeautifulSoup
from lib.parsers.baseParser import BaseParser

class DrinkLinkParser(BaseParser):
    def __init__(self):
        BaseParser.__init__(self)

    def parse(self, response):
        soup = BeautifulSoup(response.data, 'html.parser')        
        drinks = soup.select('li[class=bebidas]  a[class=shelf-item__title-link]')
        
        links = []
        for i in drinks:
            links.append(i.get('href'))
        
        return links