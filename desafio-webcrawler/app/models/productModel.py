#!/usr/bin/env python3

from lib.models.baseModel import BaseModel

class ProductModel(BaseModel):
    def __init__(self, name, url, img_url, price, availability):
        self.name = name
        self.url = url
        self.img_url = img_url
        self.price = price
        self.availability = availability

    def __str__(self):
        return 'name: {0}, url: {1}, img_url: {2}, price: {3}, availability: {4}'.format(self.name, self.url, self.img_url, self.price, self.availability)