#!/usr/bin/env python3

from lib.models.baseModel import BaseModel

class CityModel(BaseModel):
    def __init__(self, id, value):
        self.id = id
        self.value = value

    def __str__(self):
        return 'id: {0}, value: {1}'.format(self.id, self.value)