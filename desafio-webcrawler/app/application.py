#!/usr/bin/env python3

import json
from lib.jediCrawler import JediCrawler, Temp
from app.models.productModel import ProductModel
from app.parsers.cityParser import CityParser
from app.parsers.productParser import ProductParser
from app.parsers.drinkLinkParser import DrinkLinkParser

class Application:
    @staticmethod
    def run():
        url = 'https://www.comperdelivery.com.br/'

        temp = Temp(url, 'GET')

        # Encontra a lista de cidades em que é possível comprar online
        crawler = JediCrawler(temp, CityParser())
        citiesModels = crawler.action()

        for city in citiesModels:
            if city.id == 'DF':
                # Para o Distrito Federal, encontra os links bebidas listadas na home
                temp = Temp(url, 'GET', fields={'sc': city.value})
                drinkLinkParser = DrinkLinkParser()
                crawler = JediCrawler(temp, drinkLinkParser)
                drinksLink = crawler.action()
                
                products = []

                for i in (range(3)):
                    # para os três primeiros link, cria um objeto representando a bebida
                    temp = Temp(drinksLink[i], 'GET')                    
                    crawler = JediCrawler(temp, ProductParser())

                    product = crawler.action()
                    print(product)
                    products.append(product)                