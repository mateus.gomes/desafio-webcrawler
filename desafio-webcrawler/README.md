# Desafio Lett Desenvolvedor Webcrawler

 Na plataforma Comper Delivery, retornar a descrição dos três primeiros produtos para a região do Distrito Federal.

## Funcionamento básico:

 O objeto JediCrawler é criado passando a página que será usado pelo crawler(), assim como uma lista de parsers. Uma vez realizada com sucesso a request para a página, os parsers são chamados realizar a regra de
## Tecnologias utilizadas:
 * Python 3;
    * Bibliotecas:
        * Beautiful Soup para realizar parse de HTML(https://www.crummy.com/software/BeautifulSoup/bs4/doc/);
        * urllib3 para realizar requisições HTTP(https://urllib3.readthedocs.io/en/latest/);

## Observações:
 * Link da plataforma Comper Delivery(referência 30/08/2020): https://www.comperdelivery.com.br/;
 * Campo __availability__ sempre estará True. Não entrei onde essa informação estava disponível na plataforma;
