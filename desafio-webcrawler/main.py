#!/usr/bin/env python3

from app.application import Application
    
if __name__ == '__main__':
    Application.run()